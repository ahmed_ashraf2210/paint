package eg.edu.alexu.csd.oop.calculator.cs02.gui;

import eg.edu.alexu.csd.oop.calculator.cs02.myCalculator;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

	public TextField input = new TextField();
	public TextField result = new TextField();
	
	String current = "";
	
	public Button b1 = new Button();
	public Button b2 = new Button();
	public Button b3 = new Button();
	public Button b4 = new Button();
	public Button b5 = new Button();
	public Button b6 = new Button();
	public Button b7 = new Button();
	public Button b8 = new Button();
	public Button b9 = new Button();
	public Button b0 = new Button();
	public Button cl = new Button();
	public Button p = new Button();
	public Button n = new Button();
	public Button del = new Button();
	public Button cu = new Button();
	public Button dot = new Button();
	public Button s1 = new Button();
	public Button s2 = new Button();
	public Button s3 = new Button();
	public Button s4 = new Button();
	public Button e = new Button();
	
	
	
	myCalculator c = new myCalculator();
	
	public void i0() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "0";
		input.setText(current);
		result.clear();
	}
	public void i1() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "1";
		input.setText(current);
		result.clear();
	}
	public void i2() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "2";
		input.setText(current);
		result.clear();
	}
	public void i3() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "3";
		input.setText(current);
		result.clear();
	}
	public void i4() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "4";
		input.setText(current);
		result.clear();
	}
	public void i5() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "5";
		input.setText(current);
		result.clear();
	}
	public void i6() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "6";
		input.setText(current);
		result.clear();
	}
	public void i7() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "7";
		input.setText(current);
		result.clear();
	}
	public void i8() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "8";
		input.setText(current);
		result.clear();
	}
	public void i9() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "9";
		input.setText(current);
		result.clear();
	}
	public void is() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += ".";
		input.setText(current);
		result.clear();
	}
	
	
	
	public void o1() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "+";
		input.setText(current);
		result.clear();
	}
	public void o2() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "-";
		input.setText(current);
		result.clear();
	}
	public void o3() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "*";
		input.setText(current);
		result.clear();
	}
	public void o4() {
		String s = result.getText();
		if(!s.equals("")) {
			input.clear();
			current = "";
		}
		current += "/";
		input.setText(current);
		result.clear();
	}
	
	
	
	public void equal() {
		if(current.equals("")) {
			input.setPromptText("Please insert correct operation!");
			input.setStyle("-fx-border-color: red;");
		}
		else {
			c.input(current);
			input.setStyle("");
			if(c.getResult() == null) {
				input.clear();
				current = "";
				input.setPromptText("Please insert correct operation!");
				input.setStyle("-fx-border-color: red;");
			}
			else {
				result.setText(c.getResult());
			}
			
		}
		
	}
	
	
	
	public void save() {
		c.save();
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Successful Process!");
		alert.setHeaderText("Saved");
		alert.setContentText("The history is saved successfully!");

		alert.showAndWait();
	}
	public void load() {
		c.load();
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("Successful Process!");
		alert.setHeaderText("Loaded");
		alert.setContentText("The history is loaded successfully!");

		alert.showAndWait();
	}
	
	
	public void cur() {
		current = c.current();
		input.setText(current);
		result.setText(c.getResult());
	}
	public void ne() {
		current = c.next();
		if(current == null) {
			input.clear();
			input.setPromptText("There is no next operation!");
			input.setStyle("-fx-border-color: red;");
			result.clear();
			
		}
		else {
			input.setText(current);
			result.setText(c.getResult());
			
		}
		
	}
	public void pre() {
		current = c.prev();
		if(current == null) {
			input.clear();
			input.setPromptText("There is no previos operation!");
			input.setStyle("-fx-border-color: red;");
			result.clear();
		}
		else {
			input.setText(current);
			result.setText(c.getResult());
		}
		
	}
	
	public void clear() {
		current = "";
		input.clear();
		result.clear();
	}
	public void delete() {
		current = current.substring(0, current.length()-1);
		input.setText(current);
		result.clear();
	}

}
