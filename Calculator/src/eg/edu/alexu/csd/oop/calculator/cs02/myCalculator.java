/**
 * 
 */
package eg.edu.alexu.csd.oop.calculator.cs02;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.Buffer;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import eg.edu.alexu.csd.oop.calculator.Calculator;

/**
 * @author dell
 *
 */
public class myCalculator implements Calculator {

	String[] operations = new String[5];
	int current = 4;
	
	
	@Override
	public void input(String s) {
		// TODO Auto-generated method stub
		for(int i = 0; i < 4; i++) {
			operations[i] = operations[i+1];
		}
		current = 4;
		operations[current] = s;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.calculator.Calculator#getResult()
	 */
	@Override
	public String getResult() {
		// TODO Auto-generated method stub
		String s = operations[current];
		String s1;
		String s2;
		String sign;
		double re = 0;
		String result;
		
		if(s == null) {
			return null;
		}
		
		
		String regex = "((-?\\d*\\.?\\d+)|(-?\\d+\\.?\\d*))(\\+|-|\\*|/)((-?\\d*\\.?\\d+)|(-?\\d+\\.?\\d*))";
		Pattern r = Pattern.compile(regex);
		Matcher match = r.matcher(s);
		
		if (match.find( )) {
	         s1 = match.group(1);
	         sign = match.group(4);
	         s2 = match.group(5);
	         
	      }else {
	         return null;
	      }
				
		double n1 = Double.parseDouble(s1);
		double n2 = Double.parseDouble(s2);
		switch (sign) {
		case "*":
			re = n1 * n2;
			break;
		case "/":
			if(n2 == 0) {
				return null;
			}
			re = n1 / n2;
			break;
		case "+":
			re = n1 + n2;
			break;
		case "-":
			re = n1 - n2;
			break;
		}
		result = Double.toString(re);
		return result;
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.calculator.Calculator#current()
	 */
	@Override
	public String current() {
		// TODO Auto-generated method stub
		return operations[current];
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.calculator.Calculator#prev()
	 */
	@Override
	public String prev() {
		// TODO Auto-generated method stub
		if(current == 0 || operations[current-1] == null) {
			return null;
		}
		current--;
		return operations[current];
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.calculator.Calculator#next()
	 */
	@Override
	public String next() {
		// TODO Auto-generated method stub
		if(current == 4) {
			return null;
		}
		current++;
		return operations[current];
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.calculator.Calculator#save()
	 */
	@Override
	public void save() {
		// TODO Auto-generated method stub
		String fileName = "data.txt";
		File file = new File(fileName);
		try {
			file.createNewFile();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			FileWriter fileWriter = new FileWriter(file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);	
			for(int i = 0; i < 5; i++) {
				bufferedWriter.write(operations[i]+",");
			}
			bufferedWriter.newLine();
			bufferedWriter.write(Integer.toString(current));
			bufferedWriter.close();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	/* (non-Javadoc)
	 * @see eg.edu.alexu.csd.oop.calculator.Calculator#load()
	 */
	@Override
	public void load() {
		// TODO Auto-generated method stub
		String fileName = "data.txt";
		File file = new File(fileName);
		try {
			FileReader fileReader = new FileReader(file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			String [] s = new String[5];
			try {
				s = bufferedReader.readLine().split(",");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for(int i = 0; i < 5; i++) {
				if(s[i].equals("null")) {
					operations[i] = null;
				}
				else {
					operations[i] = s[i];
				}
			}
			int d = 0;
			try {
				d = Integer.parseInt(bufferedReader.readLine());
			} catch (NumberFormatException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			current = d; 
			
			
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		myCalculator c = new myCalculator();
		
		c.save();
		c.input("1+2");
		c.load();
		

	}

}
